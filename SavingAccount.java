package tema4tp;
/**
 * Clasa care extinde Accounts si din care suma nu se poate retrage
 * @author Alexandra
 *
 */
@SuppressWarnings("serial")
public class SavingAccount extends Account {

	public SavingAccount(Person a, double x) {
		super(a, x);
	}
	
	public SavingAccount() {
		// TODO Auto-generated constructor stub
	}

	public void withdraw(double a){
		System.out.println("Coulnd't withdraw from Savings.");
	}
	
}
