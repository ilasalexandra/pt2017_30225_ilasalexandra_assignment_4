package tema4tp;

import java.io.Serializable;
/**
 * Clasa care contine datele despre cont
 * @author Alexandra
 *
 */

@SuppressWarnings("serial")
public class Account implements Serializable {
	 private int accountId;
     private Person customer;
     private double balance;
     private double[] transactions= new double[100];
     private int numOfTransactions;
 /**
  * Constructorii clasei
  * @param a obicet de tip Person
  * @param id prin care se identifica contul
  * @param x soldul
 */
    public Account(int id){
    	this.accountId=id;
    }
     public Account(){
     }
     public Account(Person a, double x){
      this.setCustomer(a);
      this.balance = x;
      this.transactions = new double[100];
      this.transactions[0] = balance;
      this.numOfTransactions = 1;
     }

  public int getAccountId(){
      return accountId;
  }
  public void setAccountId(int a)
  {
	  this.accountId=a;
  }
  
  
 /**
   * Metoda care creste soldul cu o anumita valoare
   * @param amount suma pentru depunere
   * @pre amount>0
   * @post balance+=amount
 */
  public void deposit(double amount){

	  	assert amount<=0: "Amount to be deposited should be positive";
        balance = balance + amount;
        transactions[numOfTransactions] = amount;
        numOfTransactions++;
      
  }
 /**
   * Metoda care scade soldul cu o anumita valoare incluzand un comision de 2% din valoare
   * @param amount suma pentru retragere
   * @pre amount>0
   * @post balance-=amount
 */
  public void withdraw(double amount)
  {
       assert amount<=0:"Amount to be withdrawn should be positive";
        
     
          if (balance < amount) {
              System.out.println("Insufficient balance");
          } else {
              balance = balance - (amount-amount*(2/100));
              transactions[numOfTransactions] = amount;
              numOfTransactions++;
          }
      
  }
  public double getBalance(){
	  return this.balance;
  }

public Person getCustomer() {
	return customer;
}

public void setCustomer(Person customer) {
	this.customer = customer;
}


}

