package tema4tp;
/**
 * Clasa care contine detaliile despre detinatorul contului
 * @author Alexandra
 *
 */
import java.io.Serializable;

@SuppressWarnings("serial")
public class Person implements Serializable {
	
	private String personName;
	private int personId;
	
	public Person(){
		
	}
	public Person(String nume,int id){
		this.personName=nume;
		this.personId=id;
	}
	public Person(int id){

		this.personId=id;
	}
	
	public String getNumeP() {
		return personName;
	}
	public void setNumeP(String numeP) {
		this.personName = numeP;
	}
	public int getId() {
		return personId;
	}
	public void setId(int id) {
		this.personId = id;
	}
	

	
	

}
