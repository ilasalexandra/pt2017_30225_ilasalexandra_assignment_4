package tema4tp;
/**
 * Clasa care contine metodele pentru scriere in fisier si stergere din fisier
 * @author Alexandra
 *
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serialization {
	private static  File file;
	/**
	 * Metoda pentru serializarea bancii
	 * @param b de tip Bank obiectul banca care trebuie scris in fisier
	 *
	 */
	public static void serialization( Bank b)
	{
		try
		{
			file = new File("c:/Users/Alex/Desktop/Serialization.srz");
			FileOutputStream fileOut= new FileOutputStream(file);
			ObjectOutputStream out= new ObjectOutputStream(fileOut);
			out.writeObject(b);
			out.close();
			fileOut.close();
		
		}catch (IOException i)
		{
			i.printStackTrace();
		}
	}
	/**
	 * Metoda pentru deserializarea bancii
	 * @param b de tip Bank obiectul banca care trebuie citit din fisier
	 *
	 */
	public Bank deserialization()
	{
		Bank b=null;
		try
		{
			FileInputStream fileIn=new FileInputStream("C:\\bancatp4.ser");
			ObjectInputStream in=new ObjectInputStream(fileIn);
			b=(Bank) in.readObject();
			in.close();
			fileIn.close();
		} catch(IOException i) {
	        i.printStackTrace();
	     }catch(ClassNotFoundException c) {
	        System.out.println("Bank class not found");
	        c.printStackTrace();
	     }
		return b;
	}
}
