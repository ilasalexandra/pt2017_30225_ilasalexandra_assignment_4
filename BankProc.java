package tema4tp;
/**
 * Interfata implementa de clasa Bank
 * @author Alexandra
 *
 */
public interface BankProc {
	public void addPerson(String name,int id);
	public void deletePerson(int id);
	public void addAccountSave(Person p,int sum);
	public void addAccountSpend(Person p, int sum);
	public void deleteAccount(int id);
}
