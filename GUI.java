package tema4tp;
/**
 * Clasa care realizeaza interfata grafica si care face legatura cu uitilizatorul 
 * @author Alexandra
 *
 */
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import java.awt.Color;
import java.awt.EventQueue;

@SuppressWarnings("serial")
public class GUI extends JFrame {
	private JTable table_persons;
	private JTextField txtIdnume;
	private JTextField txtAccountId;
	private JTextField txtNameid;
	private JTextField txtId;
	private JTable table_accounts;
	private Bank b;
	private DefaultTableModel personGrafic;
	private DefaultTableModel accountGrafic;
	private List<Person> persoane;
	
	
	
	public void showPersons(){
		persoane=b.allPersons();
		personGrafic.setRowCount(0);
	
		for(Person p:persoane)
			{
				if(p.getNumeP()!=null)
					{
					String[] aux={Integer.toString(p.getId()),p.getNumeP()};
					personGrafic.addRow(aux);
					}
				
				}
			}
	public void showAccounts(){
		List<Account>conturi =b.allAccounts();
		accountGrafic.setRowCount(0);
		for(Account a:conturi)
		{
			String aux[]= {Integer.toString(a.getAccountId()),Integer.toString(a.getCustomer().getId()),Double.toString(a.getBalance())};
			accountGrafic.addRow(aux);
		}
	
	}
	
	public GUI(JFrame frame) {
	
		b=new Bank();
		personGrafic= new DefaultTableModel();
		accountGrafic=new DefaultTableModel();
		persoane=new ArrayList<Person>();
		frame.setBackground(Color.LIGHT_GRAY);
		frame.setLayout(null);
	
		accountGrafic.addColumn("ID");
		accountGrafic.addColumn("ID PERSON");
		accountGrafic.addColumn("BALANCE");
		
		personGrafic.addColumn("ID");
		personGrafic.addColumn("NAME");
		table_accounts = new JTable(accountGrafic);
		table_accounts.setBounds(409, 36, 236, 140);
		table_accounts.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				if(e.getClickCount()==1)
					{
						if(table_accounts.getRowCount()==1)
						{
							System.out.println("There is "+ table_accounts.getRowCount()+ " account");
						}
						else	
							System.out.println("There are "+ table_accounts.getRowCount()+ " accounts");
			}}
		});
		frame.add(table_accounts);
		
		JLabel lblAccounts = new JLabel("Persons");
		lblAccounts.setBounds(516, 176, 56, 16);
		frame.add(lblAccounts);
		
		JLabel lblPersons = new JLabel("Accounts");
		lblPersons.setBounds(516, 13, 56, 16);
		frame.add(lblPersons);
		
	   
		
   
		
		table_persons = new JTable(personGrafic);
		table_persons.setBounds(409, 205, 236, 140);
		table_persons.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				if(e.getClickCount()==1)
				{
					if(table_persons.getRowCount()==1)
					{
						System.out.println("There is "+ table_persons.getRowCount()+ " person");
					}
					else	
						System.out.println("There are "+ table_persons.getRowCount()+ " persons");
			}}
		});
		
		frame.add(table_persons);
		
		txtIdnume = new JTextField();
		txtIdnume.setText("id,sum");
		txtIdnume.setBounds(12, 37, 116, 22);
		frame.add(txtIdnume);
		txtIdnume.setColumns(10);
		
		JButton btnAddSavingaccount = new JButton("Add SavingAccount");
		btnAddSavingaccount.setBounds(140, 36, 146, 25);
		btnAddSavingaccount.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				String a= txtIdnume.getText();
				StringTokenizer stok = new StringTokenizer(a, ",");
			    String tokens[] = new String[stok.countTokens()];
			    for(int i=0; i<tokens.length; i++)
			        tokens[i] = stok.nextToken();
				int id= Integer.parseInt(tokens[0]);
				int sum=Integer.parseInt(tokens[1]);
				Person p=new Person(id);
				b.addAccountSave(p, sum);
			}
		});
		frame.add(btnAddSavingaccount);
		
		JButton btnAddSavingaccount_1 = new JButton("Add SpendingAccount");
		btnAddSavingaccount_1.setBounds(140, 61, 146, 25);
		btnAddSavingaccount_1.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				String a= txtIdnume.getText();
				StringTokenizer stok = new StringTokenizer(a, ",");
			    String tokens[] = new String[stok.countTokens()];
			    for(int i=0; i<tokens.length; i++)
			        tokens[i] = stok.nextToken();
				int id= Integer.parseInt(tokens[0]);
				int sum=Integer.parseInt(tokens[1]);
				Person p=new Person(id);
				b.addAccountSpend(p, sum);
			}
		});
		frame.add(btnAddSavingaccount_1);
		
		txtAccountId = new JTextField();
		txtAccountId.setText("acc id");
		txtAccountId.setBounds(12, 113, 116, 22);
		frame.add(txtAccountId);
		txtAccountId.setColumns(10);
		
		JButton btnDeleteAccount = new JButton("Delete Accounts");
		btnDeleteAccount.setBounds(140, 112, 146, 25);
		btnDeleteAccount.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				int id=Integer.parseInt(txtAccountId.getText());
				b.deleteAccount(id);
			}
		});
		frame.add(btnDeleteAccount);
		
	
		
	
		
		txtNameid = new JTextField();
		txtNameid.setText("name,id");
		txtNameid.setBounds(12, 271, 116, 22);
		frame.add(txtNameid);
		txtNameid.setColumns(10);
		
		JButton btnShowPerson = new JButton("Show Persons");
		btnShowPerson.setBounds(700, 270, 146, 25);
		btnShowPerson.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
			showPersons();
			}
		});
		frame.add(btnShowPerson);
		
		JButton btnShowAccounts = new JButton("Show Accounts");
		btnShowAccounts.setBounds(700, 61, 146, 25);
		btnShowAccounts.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
			showAccounts();
			}
		});
		frame.add(btnShowAccounts);
		
		JButton btnAddPerson = new JButton("Add Person");
		btnAddPerson.setBounds(140, 270, 97, 25);
		btnAddPerson.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				String s=txtNameid.getText();
				StringTokenizer stok = new StringTokenizer(s, ",");
			    String tokens[] = new String[stok.countTokens()];
			    for(int i=0; i<tokens.length; i++)
			        tokens[i] = stok.nextToken();
			    String name=tokens[0];
			    int id= Integer.parseInt(tokens[1]);
				b.addPerson(name, id);
			}
		});
		frame.add(btnAddPerson);
		
		txtId = new JTextField();
		txtId.setText("id");
		txtId.setBounds(12, 306, 116, 22);
		frame.add(txtId);
		txtId.setColumns(10);
		
		JButton btnDeletePerson = new JButton("Delete Person");
		btnDeletePerson.setBounds(139, 305, 147, 25);
		btnDeletePerson.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				int id= Integer.parseInt(txtId.getText());
				b.deletePerson(id);
			}
		});
		frame.add(btnDeletePerson);
		
		JTextField withdraw=new JTextField("acc id,sum");
		withdraw.setBounds(12, 410, 116, 22);
		frame.add(withdraw);
		
		JButton withdrawbtn =new JButton("Withdraw");
		withdrawbtn.setBounds(139,410,147, 25);
		withdrawbtn.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				String a= withdraw.getText();
				StringTokenizer stok = new StringTokenizer(a, ",");
			    String tokens[] = new String[stok.countTokens()];
			    for(int i=0; i<tokens.length; i++)
			        tokens[i] = stok.nextToken();
				int id= Integer.parseInt(tokens[0]);
				double sum=Double.parseDouble(tokens[1]);
				Account ac=new Account(id);
				b.withdraw(ac, sum);
			}
		});
		frame.add(withdrawbtn);
		frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Serialization.serialization(b);
                e.getWindow().dispose();
            }
        });
		
		
	}

	

	
	public static void main(String[] args) {
		JFrame frame=new JFrame();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
				try {
					GUI window = new GUI(frame);
					frame.setVisible(true);
					//this.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		})
		
		;
		
		
		
		
		
	}
	
	
	
}
