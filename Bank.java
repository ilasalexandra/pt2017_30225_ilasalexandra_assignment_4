package tema4tp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
/**
 * Clasa care realizeaza operatiile necesare managemntului bancar
 * @author Alexandra
 *
 */

@SuppressWarnings("serial")
public class Bank implements Serializable,BankProc{

	private HashMap<Person, List<Account>> banca;  
	private int accountCount;

/**
  * Constructor
  * 
  *
 */
	
	public Bank(){
		this.banca=new HashMap<Person,List<Account>>();
		this.accountCount=0;
	}
	
/**
  * Metoda pentru adaugarea persoanei in banca
  * @param name numele persoanei
  * @param id id-ul persoanei
  * 
*/
	
	public void addPerson(String name,int id)
	{	Person p= new Person(name,id);
			if(banca.containsKey(p)==false)
				{
					banca.put(p, null);
					System.out.println("A person with id "+ id + " was added");
				}
			else
				System.out.println("Person already added");
	}
	
/**
  * Metoda pentru afisarea tuturor persoanelor din banca
  * 
  *
*/
	 public List<Person> allPersons(){
		List<Person> allPersons=new ArrayList<Person>();
		
		for(Entry<Person,List<Account>> a:banca.entrySet())
		{
			allPersons.add(a.getKey());
		}
		return allPersons;
	 }
	 
/**
  * Metoda pentru adaugarea unui cont de tip savings
  * @param p de tip Person persoana careia i se adauga contul
  * @param sum suma depusa
*/
	 
	 public void addAccountSave(Person p,int sum){
		int ok=0;
		 Account a= new SavingAccount();
		 a.setCustomer(p);
		 a.setAccountId(this.accountCount++);
		 a.deposit(sum);
		 List<Account> conturi= banca.get(p);
		 if(conturi==null)
		 {
			
			 List<Account> cont=new ArrayList<Account>();
			 cont.add(a);
			 banca.put(p, cont);
			 ok=1;
		 }
		 else
			 {
			 	banca.get(p).add(a);
			 	ok=1;
			 }
		 	if(ok==1)
			System.out.println("A new Savings Account with id "+a.getAccountId()+" was added to the person with id "+ p.getId()+" Balance " +a.getBalance());
		 	
	 }
		 
/**
  * Metoda pentru adaugarea unui cont de tip spendings
  * @param p de tip Person persoana careia i se adauga contul
  * @param sum suma depusa
*/		  

	 public void addAccountSpend(Person p, int sum){
		 int ok=0;
		 Account a= new SpendingAccount();
		 a.setAccountId(this.accountCount++);
		 a.setCustomer(p);
		 a.deposit(sum);
		 List<Account> conturi= banca.get(p);
		 if(conturi==null)
		 {
			 ok=1;
			 List<Account> cont=new ArrayList<Account>();
			 cont.add(a);
			 banca.put(p, cont);
		 }
		 else
			 {
			 	ok=1;
			 	conturi.add(a);
			 	banca.put(p, conturi);
			 }
		 	if(ok==1)
			System.out.println("A new Spending Account with id "+a.getAccountId()+" was added to the person with id "+ p.getId()+" Balance " +a.getBalance());
}
	 
/**
  * Metoda pentru afisarea tuturor conturilor din banca
  * 
  *
 */
	 public List<Account> allAccounts(){
		 List<Account> lista =new ArrayList<Account>();
		 for(Entry<Person,List<Account>> a:banca.entrySet())
		 {
			 if(a.getValue()!=null)
			 {
				 for(Account ac:a.getValue())
					 lista.add(ac);
			 }
		 }
		 
		 return lista;
		
	 }
	 
/**
  * Metoda pentru stergerea unui cont
  * @param id de tip int id-ul contului ce trebuie sters
  *
*/
	public void deleteAccount(int id){
		int ok=0;
		outerloop:for(Entry<Person,List<Account>> a:banca.entrySet())
			for(Account ac:a.getValue())
			{
				if(ac.getAccountId()==id)
					{	
						ok=1;
						a.getValue().remove(ac);
						System.out.println("Account with id "+id+" was deleted");
						break outerloop;
					}
				
			}
		if(ok==0)
			System.out.println("Account not found");
			
		
	}
/**
  * Metoda pentru stergerea unei persoane
  * @param id de tip int id-ul persoanei ce trebuie sters
  *
*/
	
	public void deletePerson(int id){
	
		Person p=new Person(id);
		for(Entry<Person,List<Account>> a:banca.entrySet())
		{
			if(a.getKey().getId()==p.getId())
				{
					banca.remove(p,a.getValue());
					System.out.println("Person with id "+ id+ " was deleted");
				}
		}
	}
	
/**
  * Metoda pentru retragerea dintr-un cont din banca
  * @param ac de tip Account contul din care trebuie retrasa suma
  *@sum de tip double suma ce trebuie retrasa
*/
	
	public void withdraw(Account ac,double sum)
	{
		int ok=0;
		List<Account>lista=new ArrayList<Account>();
		outerloop:for(Entry<Person,List<Account>> a:banca.entrySet())
			if(a!=null)
				
			{	lista=a.getValue();
				if(lista!=null)
				{	
					for(Account aux:a.getValue())
						if(aux.getAccountId()==ac.getAccountId())
						{
							ok=1;
							aux.withdraw(sum);
							break outerloop;
						}
			}	}
		if(ok==1)
			System.out.println("Withdrawal of " + sum +" from account with id " + ac.getAccountId() +" Was successfull");
;
		
	}
	
	
	
	
	
	
}
